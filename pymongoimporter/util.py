from functools import partial
from json import JSONDecoder


def remove_unwanted_characters(buffer: str) -> str:
    try:
        if buffer[0] == ",":
            buffer = buffer[1:]
        while buffer[0].isspace():
            buffer = buffer[1:]
    except IndexError:
        pass
    finally:
        return buffer


def json_parse(fileobj, decoder=JSONDecoder(), buffersize=2048):
    buffer = ""
    for chunk in iter(partial(fileobj.read, buffersize), ""):
        buffer += chunk
        buffer = remove_unwanted_characters(buffer)
        while buffer:
            try:
                result, index = decoder.raw_decode(buffer)
                yield result
                buffer = buffer[(index):].lstrip()
            except ValueError as e:
                # Not enough data to decode, read more
                break
            else:
                # Successfully parsed, remove following unnecessary characters
                buffer = remove_unwanted_characters(buffer)
