import argparse
import asyncio
import sys

import bson
import motor.motor_asyncio

from settings import *
from pymongoimporter import util

client = motor.motor_asyncio.AsyncIOMotorClient(CONNECTION_STRING)


async def process_file(db, input_file: str):
    with open(input_file, "r") as file:
        if not file.read(1).startswith("["):
            file.seek(0)
        docs = []
        cnt = 0

        futures = []
        for doc in util.json_parse(file):
            if "_id" in doc:
                doc["_id"] = bson.ObjectId(doc["_id"]["$oid"])
            cnt += 1
            docs.append(doc)

            if cnt >= BATCH_SIZE:
                futures.append(db.insert_many(docs, ordered=False))
                if len(futures) > 20:
                    new_futures = []
                    for future in futures:
                        if not future.done():
                            new_futures.append(future)
                    futures = new_futures
                    await asyncio.sleep(0.01)

                cnt = 0
                docs = []
        else:
            futures.append(db.insert_many(docs, ordered=False))
            for future in futures:
                await future


def main():
    parser = argparse.ArgumentParser(
        prog="PyMongoImport",
        description="Import your JSONs into MongoDB",
        epilog="Thanks for using this tool. Please contribute if any functionality is missing!",
    )

    parser.add_argument("database")
    parser.add_argument("collection")
    parser.add_argument("input_file")

    args = parser.parse_args()

    input_file = args.input_file
    database_name = args.database
    collection_name = args.collection

    print(database_name, collection_name)
    db = client.__getattr__(database_name).__getattr__(collection_name)
    print(db)

    asyncio.run(process_file(db, input_file))


if __name__ == "__main__":
    main()
