# Pymongoimporter
###### A simple and fast alternative to mongoimport for importing JSON into MongoDB

## 👔 Usage
```shell
pymongoimport <filename>
```
example
```shell
pymongoimport test.json
```

## ⚙️ Installation
```shell
git clone https://gitlab.com/mykola.b/pymongoimporter.git
cd pymongoimporter
poetry install
```
Done 🎉
